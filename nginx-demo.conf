# For more information on configuration, see:
#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/

user nginx;
worker_processes auto;
error_log /var/log/nginx/error.log notice;
pid /run/nginx.pid;

# Load dynamic modules. See /usr/share/doc/nginx/README.dynamic.
include /usr/share/nginx/modules/*.conf;

events {
    worker_connections 1024;
}

http {
    log_format stage_log '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    #access_log  /var/log/nginx/access.log  main;

    log_format prod_log '{"Request": "$request", "Status": "$status", "Request_URI": "$request_uri", "Host": "$host", "Client_IP": "$remote_addr", "Proxy_IP(s)": "$proxy_add_x_forwarded_for","Proxy_Hostname": "$proxy_host","Real_IP": "$http_x_real_ip", "User_Client": "$http_user_agent","TimeTaken": "$request_time", "ContentType": "$content_type"}';


    sendfile            on;
    tcp_nopush          on;
    keepalive_timeout   65;
    types_hash_max_size 4096;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    # Load modular configuration files from the /etc/nginx/conf.d directory.
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    # for more information.
    include /etc/nginx/conf.d/*.conf;

    server {
        listen      80;
        listen      [::]:80;
        server_name _;
        access_log /var/log/nginx/access_log.log prod_log;
        return 301 https://$host$request_uri;
    }

    
    server {
        listen       443 ssl http2;
        listen       [::]:443 ssl http2;
        server_name  _;
        root         /usr/share/nginx/html;
        ssl_certificate "/etc/pki/nginx/cert-prod.pem";
        ssl_certificate_key "/etc/pki/nginx/key-prod.pem";

        access_log /var/log/nginx/access_log.log prod_log;
        location / {
            return 403;
        }
    }

    server {
        listen       443 ssl http2;
        listen       [::]:443 ssl http2;
        server_name  prod.devopsdemo.com;                # here i am using self-signed cert
        root         /usr/share/nginx/html;

        ssl_certificate "/etc/pki/nginx/cert-prod.pem";
        ssl_certificate_key "/etc/pki/nginx/key-prod.pem";
        
        add_header Strict-Transport-Security "max-age=15552000; includeSubDomains" always;
        add_header X-Frame-Options SAMEORIGIN;
        proxy_hide_header X-Powered-By;
        proxy_hide_header X-Forwarded-Host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        server_tokens off;
        etag off;
        access_log /var/log/nginx/access_log.log prod_log;

        error_log /var/log/nginx/error_log.log;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        error_page 404 /404.html;
        location = /404.html {
        }

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
        }

        location ~ ^/app1/(about)$ {  # only app1/about
            proxy_pass http://127.0.0.1:40000;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        }

        location ~ ^/app2/(hello)$ {    # only app2/hello
            proxy_pass http://127.0.0.1:50000;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        }

        location ~ ^/app3/(hello/login|about)$ {    # only app3/hello/login and app3/about
            proxy_pass http://127.0.0.1:30000;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        }
    }


    server {
        listen       443 ssl http2;
        listen       [::]:443 ssl http2;
        server_name  stage.devopsdemo.com;                # here i am using self-signed cert
        root         /usr/share/nginx/html;

        ssl_certificate "/etc/pki/nginx/cert-stage.pem";
        ssl_certificate_key "/etc/pki/nginx/key-stage.pem";
        
        add_header Strict-Transport-Security "max-age=15552000; includeSubDomains" always;
        add_header X-Frame-Options SAMEORIGIN;
        proxy_hide_header X-Powered-By;
        proxy_hide_header X-Forwarded-Host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        server_tokens off;
        etag off;
        access_log /var/log/nginx/access_log.log stage_log;

        error_log /var/log/nginx/error_log.log;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        error_page 404 /404.html;
        location = /404.html {
        }

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
        }

        location ~ ^/app1/(hello|about)$ {
            proxy_pass http://127.0.0.1:40000;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        }

        location ~ ^/app2/(hello)$ {
            proxy_pass http://127.0.0.1:50000;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        }

        location ~ ^/app3/(hello/login|hello|about)$ {
            proxy_pass http://127.0.0.1:30000;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        }
    }
}

